package com.test.blubblub.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.test.blubblub.entities.City

/**
 * Created by KlemenG on 26. 10. 2021.
 */
@Dao
interface CityDao {
    @Query("SELECT * FROM city")
    fun getAllLive(): LiveData<MutableList<City>>

    @Query("SELECT * FROM city")
    suspend fun getAll(): MutableList<City>

    @Insert
    suspend fun insertAll(city: City)

    @Update
    suspend fun update(city: City)

    @Delete
    suspend fun delete(city: City)
}