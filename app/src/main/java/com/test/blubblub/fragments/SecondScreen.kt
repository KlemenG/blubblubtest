package com.test.blubblub.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.test.blubblub.databinding.FragmentSecondScreenBinding
import com.test.blubblub.viewModels.CityViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class SecondScreen : Fragment(), View.OnClickListener {
    private var _binding: FragmentSecondScreenBinding? = null
    private val binding get() = _binding!!

    private val cityViewModel: CityViewModel by activityViewModels {
        ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondScreenBinding.inflate(inflater, container, false)

        binding.btnCancel.setOnClickListener(this)
        binding.btnFinish.setOnClickListener(this)

        return binding.root
    }

    override fun onClick(v: View?) {
        if (v == binding.btnCancel) {
            requireActivity().onBackPressed()
        } else if (v == binding.btnFinish) {
            lifecycleScope.launch(Dispatchers.IO) {
                cityViewModel.insertCity(getCityNameFromInput())
                withContext(Dispatchers.Main) {
                    Toast.makeText(context, "City successfully added!", Toast.LENGTH_LONG).show()
                    requireActivity().onBackPressed()
                }
            }
        }
    }

    private fun getCityNameFromInput(): String {
        return binding.etCityName.text.trim().toString()
    }
}