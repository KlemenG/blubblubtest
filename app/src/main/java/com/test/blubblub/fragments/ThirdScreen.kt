package com.test.blubblub.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.test.blubblub.databinding.FragmentThirdScreenBinding
import com.test.blubblub.viewModels.CityViewModel

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class ThirdScreen : Fragment() {
    private var _binding: FragmentThirdScreenBinding? = null
    private val binding get() = _binding!!

    private val cityViewModel: CityViewModel by activityViewModels {
        ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentThirdScreenBinding.inflate(inflater, container, false)

        cityViewModel.getWeatherLive().observe(viewLifecycleOwner) { weather ->
            binding.tvCityName.text = weather.cityName
            binding.tvWeatherInfo.text = weather.weatherInfo
            binding.tvWeatherDescription.text = weather.description
        }

        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }

        return binding.root
    }

}