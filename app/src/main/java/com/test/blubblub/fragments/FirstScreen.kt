package com.test.blubblub.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.blubblub.R
import com.test.blubblub.adapters.CityListAdapter
import com.test.blubblub.databinding.FragmentFirstScreenBinding
import com.test.blubblub.viewModels.CityViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class FirstScreen : Fragment() {
    private var _binding: FragmentFirstScreenBinding? = null
    private val binding get() = _binding!!

    private val cityViewModel: CityViewModel by activityViewModels {
        ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstScreenBinding.inflate(inflater, container, false)

        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = CityListAdapter(mutableListOf(), { city ->
            cityViewModel.getWeatherDataForCity(city)
            findNavController().navigate(R.id.action_firstScreen_to_thirdScreen)
        }) { city ->
            lifecycleScope.launch(Dispatchers.IO) {
                cityViewModel.deleteCity(city)
            }
        }

        binding.addButton.setOnClickListener { findNavController().navigate(R.id.action_firstScreen_to_secondScreen) }

        cityViewModel.refreshAllCities()
        cityViewModel.getAllCities().observe(viewLifecycleOwner) { city ->
            val adapter = binding.recyclerView.adapter as? CityListAdapter
            adapter?.setData(city)
        }
        return binding.root
    }
}