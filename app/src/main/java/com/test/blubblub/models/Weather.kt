package com.test.blubblub.models

/**
 * Created by KlemenG on 26. 10. 2021.
 */
data class Weather(
    val cityName: String?,
    val weatherInfo: String,
    val description: String,
    val temperature: String
)