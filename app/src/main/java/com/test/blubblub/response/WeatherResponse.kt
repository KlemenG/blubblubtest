package com.test.blubblub.response

data class WeatherResponse(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)