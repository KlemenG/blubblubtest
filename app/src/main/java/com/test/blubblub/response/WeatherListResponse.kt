package com.test.blubblub.response

data class WeatherListResponse(
    val weather: List<WeatherResponse>,
    val main: TemperatureResponse
)