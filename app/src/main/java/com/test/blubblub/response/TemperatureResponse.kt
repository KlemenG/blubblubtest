package com.test.blubblub.response

data class TemperatureResponse(
    val temp: String
)
