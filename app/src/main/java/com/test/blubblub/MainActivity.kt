package com.test.blubblub

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.blubblub.databinding.ActivityMainBinding

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}