package com.test.blubblub.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.room.Room
import com.google.gson.Gson
import com.test.blubblub.AppDatabase
import com.test.blubblub.entities.City
import com.test.blubblub.models.Weather
import com.test.blubblub.response.TemperatureResponse
import com.test.blubblub.response.WeatherListResponse
import com.test.blubblub.response.WeatherResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import okio.IOException
import timber.log.Timber

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class CityViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val API_KEY = "569bb88e278e30aa8629b99effeb3862"
    }

    private val weatherLive: MutableLiveData<Weather> = MutableLiveData()
    private val db = Room.databaseBuilder(
        application,
        AppDatabase::class.java, "database-name"
    ).build()

    private val client = OkHttpClient()
    private val cityDao = db.cityDao()

    fun getAllCities(): LiveData<MutableList<City>> = cityDao.getAllLive()

    fun refreshAllCities() {
        viewModelScope.launch(Dispatchers.IO) {
            cityDao.getAll().forEach { city ->
                updateCityWeatherData(city)
            }
        }
    }

    suspend fun deleteCity(city: City) {
        cityDao.delete(city)
    }

    suspend fun insertCity(cityNameFromInput: String) {
        val city = City(null, cityNameFromInput, null, null, null)
        cityDao.insertAll(city)
    }

    private fun getCityWeatherFromServer(
        cityName: String?,
        successCallback: (response: Response) -> Unit
    ) {
        val url =
            "https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$API_KEY"
        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                successCallback(response)
            }
        })
    }

    private fun updateCityWeatherData(city: City) {
        getCityWeatherFromServer(city.cityName) { response ->
            val weatherJson = response.body?.string()
            val gson = Gson()
            try {
                val weather = gson.fromJson(weatherJson, WeatherListResponse::class.java)
                val firstResult = weather.weather[0]
                city.description = firstResult.description
                city.weatherInfo = firstResult.main
                city.temperature = weather.main.temp
                viewModelScope.launch(Dispatchers.IO) {
                    cityDao.update(city)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getWeatherDataForCity(city: City) {
        getCityWeatherFromServer(city.cityName) { response ->
            val weatherJson = response.body?.string()
            val gson = Gson()
            try {
                val weather = gson.fromJson(weatherJson, WeatherListResponse::class.java)
                weatherLive.postValue(
                    mapWeatherModel(
                        weather.weather[0],
                        city.cityName ?: "",
                        weather.main
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

            Timber.d("Response: $weatherJson")
        }
    }

    fun getWeatherLive(): LiveData<Weather> {
        return weatherLive
    }

    private fun mapWeatherModel(
        response: WeatherResponse,
        city: String,
        temperatureResponse: TemperatureResponse
    ): Weather {
        return Weather(
            cityName = city,
            weatherInfo = response.main,
            description = response.description,
            temperature = temperatureResponse.temp
        )
    }
}