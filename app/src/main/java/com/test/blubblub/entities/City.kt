package com.test.blubblub.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by KlemenG on 26. 10. 2021.
 */
@Entity
data class City(
    @PrimaryKey(autoGenerate = true) val uid: Int? = null,
    @ColumnInfo(name = "city_name") var cityName: String?,
    @ColumnInfo(name = "weather_info") var weatherInfo: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "temperature") var temperature: String?
)