package com.test.blubblub

import android.app.Application
import timber.log.Timber

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class BlubBlubTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}