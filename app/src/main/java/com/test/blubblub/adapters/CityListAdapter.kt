package com.test.blubblub.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.blubblub.databinding.ItemCityListBinding
import com.test.blubblub.entities.City

/**
 * Created by KlemenG on 26. 10. 2021.
 */
class CityListAdapter(
    private var listOfCities: MutableList<City>,
    val itemClickCallback: (city: City) -> Unit,
    val itemLongPressCallback: (city: City) -> Unit
) : RecyclerView.Adapter<CityListAdapter.ViewHolder>() {
    @SuppressLint("NotifyDataSetChanged")
    fun setData(listOfCities: MutableList<City>) {
        this.listOfCities.clear()
        this.listOfCities.addAll(listOfCities)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemCityListBinding.inflate(inflater, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = listOfCities[position]
        holder.tvCityName.text = "${city.cityName}  ${city.temperature}"
        holder.cardView.setOnClickListener { itemClickCallback(city) }
        holder.cardView.setOnLongClickListener {
            itemLongPressCallback(city)
            true
        }
    }

    override fun getItemCount(): Int = listOfCities.size

    class ViewHolder(
        binding: ItemCityListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val tvCityName = binding.tvCityName
        val cardView = binding.cardView
    }
}