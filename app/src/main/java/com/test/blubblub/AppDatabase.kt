package com.test.blubblub

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.blubblub.dao.CityDao
import com.test.blubblub.entities.City

/**
 * Created by KlemenG on 26. 10. 2021.
 */
@Database(entities = [City::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityDao(): CityDao
}